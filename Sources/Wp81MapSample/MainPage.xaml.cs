﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace Wp81MapSample
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.bytesTask = LoadBytes();
            var customSource = new CustomMapTileDataSource();
            customSource.BitmapRequested += OnBitmapRequested;
            var tileSource = new MapTileSource(customSource);
            this.mapControl.TileSources.Add(tileSource);
        }

        private Task<byte[]> bytesTask;

        private async Task<byte[]> LoadBytes()
        {
            IStorageFile nativeFile = await Package.Current.InstalledLocation.GetFileAsync("SampleTile.png"); //  "SimpleTile.png");
            using (var stream = await nativeFile.OpenAsync(FileAccessMode.Read))
            {
                var decoder = await Windows.Graphics.Imaging.BitmapDecoder.CreateAsync(stream);
                var pixelProvider = await decoder.GetPixelDataAsync(
                        Windows.Graphics.Imaging.BitmapPixelFormat.Rgba8,
                        Windows.Graphics.Imaging.BitmapAlphaMode.Straight,
                        new Windows.Graphics.Imaging.BitmapTransform(),
                        Windows.Graphics.Imaging.ExifOrientationMode.RespectExifOrientation,
                        Windows.Graphics.Imaging.ColorManagementMode.ColorManageToSRgb);

                var pixels = pixelProvider.DetachPixelData();
                return pixels;
            }
        }

        private readonly object syncObject = new object();

        private int actualRequestsCount;

        private async void OnBitmapRequested(CustomMapTileDataSource sender, MapTileBitmapRequestedEventArgs args)
        {
            var defferal = args.Request.GetDeferral();
            try
            {
                lock (syncObject)
                {
                    actualRequestsCount++;
                    Debug.WriteLine("Actual requests: " + actualRequestsCount + ". request started");
                }

                // Simulate long loading.
                await Task.Delay(8000);

                // Load tile here.
                await LoadTile(args);

                lock (syncObject)
                {
                    actualRequestsCount--;
                    Debug.WriteLine("Actual requests: " + actualRequestsCount + ". request completed");
                }
            }
            finally
            {
                defferal.Complete();
            }
        }

        private async Task LoadTile(MapTileBitmapRequestedEventArgs args)
        {
            byte[] pixels = null;

            pixels = await this.bytesTask;
            var randomAccessStream = new InMemoryRandomAccessStream();
            var outputStream = randomAccessStream.GetOutputStreamAt(0);
            var writer = new DataWriter(outputStream);
            writer.WriteBytes(pixels);
            await writer.StoreAsync();
            await writer.FlushAsync();

            args.Request.PixelData = RandomAccessStreamReference.CreateFromStream(randomAccessStream);
        }


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }
    }
}
